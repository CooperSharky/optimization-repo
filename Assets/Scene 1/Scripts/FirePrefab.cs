﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FirePrefab : MonoBehaviour
{
    //What to shoot, and how fast. 
    public GameObject Prefab;
    private Camera mainCam;
    public float FireSpeed = 5;

    private void Start()
    {

        mainCam = GetComponent<Camera>(); 

    
    }


    // On update, check to see if the player clicks the mouse.
    // if so, the PREFAB is instantiated, and is given a Vector based on mouse position. 
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            //Camera could be gotten outside of update!
            Vector3 clickPoint = mainCam.ScreenToWorldPoint(Input.mousePosition + Vector3.forward);
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);

            //Another GetComponent that could be obtained outside of Update 
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }
}
