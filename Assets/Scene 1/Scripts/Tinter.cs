﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    //A script that takes all of its childrens' renderers' materials, and gives them a new randomized color. 

    public bool self = true; 
    void Start()
    {
        //If not a parent, just color yourself
        if (self)
        {
            GetComponent<Renderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            Destroy(this); 
        }


        //Colour all children a random color
        foreach (Transform child in transform)
            child.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
 
            Destroy(this);
    }

}
