﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{

    private Camera camera;
    private NavMeshAgent myNAvMeshAgent;

    // THe mouse casts a ray every frame, and if it touches the ground, have this NavMesh'd gameobject move towards it.

    //   //This could (and should) not be an update. You could make this an Enumerator. 
    //   void Update ()
    //   {
    //       //A GAMEOBJECT.FIND IN UPDATE WHAT, WHAT, PLEASE GET YOUR FILTHY CAMERA SOMEWHERE ELSE 
    //       Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
    //       RaycastHit hit = new RaycastHit();
    //       if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
    //       {
    //           //Also getting the NMA every frame when you don't have to 
    //           GetComponent<NavMeshAgent>().destination = hit.point;
    //       }
    //}


    private void Start()
    {
        camera = Camera.main;
        myNAvMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                //Also getting the NMA every frame when you don't have to 
                myNAvMeshAgent.destination = hit.point;
            }
        }
    }
}
