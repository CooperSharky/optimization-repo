﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    

    ////This right here. This is just checking to see if the player enters the radius 
    ////it gets EVERY COLLIDER, EVERY FRAME, using OVERLAP SPHERE, and TAG CHECKS ALL OF THEM with a STRING 
    ////Fredd this is your most dastardly work yet 
    //public void Update()
    //{
    //    //An overlapSphere in update. These are really expensive. 
    //    Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
    //    for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
    //    {
    //        //We're really out here comparing tags with every collider every single frame huh. 
    //        if ( collidingColliders[colliderIndex].tag == "Player" )
    //        {
    //            Destroy( this.gameObject );
    //        }
    //    }
    //}

    //Just do a trigger collider of the same size. 
    //When colliding with the player tag, trigger any respawn functionality. 

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }

}
