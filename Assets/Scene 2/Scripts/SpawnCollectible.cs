﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    //A manager for collectibles. Ensures that a collectible exists


    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // A somewhat unecessary update - checks EVERY FRAME if the current collectible is still around, and instantiates a new one.
    //Not the worst Update we've seen here, though. 
    void Update()
    {
        if ( m_currentCollectible == null )
        {
            //It gets a random child and puts it at that transform. These children could be in an array, and it might be a bit cheaper. 
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
        }
    }
}
